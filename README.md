# Inductive Representation Learning on Large Graphs
In this work we replicate the referenced [paper](https://arxiv.org/pdf/1706.02216.pdf) and introduce a **Softmax** aggregator for learning the graph features. A detailed report can be find under `documents/report.pdf`.

## How to Run 
You only need to run the `graphsage_training.ipynb` in Google Colab and make sure changing the runtime type into a GPU. You can also run the preprocessing notebooks in Google Colab, but you don't have to, since the preprocessed datasets are already included in the training notebook.

## Datasets
We compare our results with the results represented in [this paper](https://arxiv.org/pdf/1706.02216.pdf) on the [ppi dataset](http://snap.stanford.edu/graphsage/ppi.zip) and provide other experiments on the [ogbn-proteins dataset](https://snap.stanford.edu/ogb/data/nodeproppred/proteinfunc.zip) as well as the [CORA dataset](https://docs.dgl.ai/en/0.4.x/api/python/data.html#cora-citation-network-dataset). To unify the input format of the graphs we [here](https://seafile.cloud.uni-hannover.de/f/e9a1afe36c224b6094bc/?dl=1) provide the preprocessed datasets.

## Experimental Results over the micro-averaged F1-Scores

| - | Mean | LSTM | MaxPool | MeanPool | GCN | Softmax  |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| dgl-cora | 0.520 | 0.407 | 0.102 | 0.520 | 0.623 | 0.37269 |
| ogbn-proteins | 0.157 | 0.163 | 0.149 | 0.180 | 0.122 | 0.152 |
| ppi | 0.585 | 0.604 | 0.607 | 0.579 | 0.521 | 0.564 |
| ppi (original) | 0.598 | 0.612 | 0.600 | 0.600 | 0.500 | -- |

## Contribution
This work has been done within the mini-project of the Deep Learning course in SoSe20 at the Leibniz University of Hanover
by the Computer Science master students:
* Muaid Mughrabi (muaid.mughrabi@gmail.com)
* Omar Arab Oghli (omar.araboghli@outlook.com)
* Lars Wiegmann (lwiegmann@htp-tel.de)

## References
* [Paper](https://arxiv.org/pdf/1706.02216.pdf)
* Tensorflow [implementation](https://github.com/williamleif/GraphSAGE)
* [PPI](http://snap.stanford.edu/graphsage/ppi.zip) dataset
* [OGBN-Proteins](https://snap.stanford.edu/ogb/data/nodeproppred/proteinfunc.zip) dataset
* [CORA](https://docs.dgl.ai/en/0.4.x/api/python/data.html#cora-citation-network-dataset) dataset
